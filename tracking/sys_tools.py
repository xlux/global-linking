import subprocess
import sys

def run_procedure(args):

    
    status_output = subprocess.call(args.split(' '))
    if status_output != 0:
        print(f'ERROR: procedure ${args} failed with an output {status_output}')

    return status_output
