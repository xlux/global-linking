"""
Author: Filip Lux (2023), Masaryk University
Licensed under MIT License
"""

import os
import shutil
from pathlib import Path
from tqdm import tqdm
from skimage import io
import numpy as np

from .graph import FlowGraph
from .sys_tools import run_procedure

def run_libct(res_path,
              idx_min=0):
        
    # copy tracking.txt to libct
    source_file = Path(res_path, 'tracking.txt')
    source_solution = Path(res_path, 'tracking.sol')
    assert os.path.isfile(source_file), source_file
    
    local_input = 'tracking.txt' 
    local_output = 'tracking.sol'
    
    # copy to local
    shutil.copyfile(source_file, local_input)
    
    #show_libct(local_input, z_shift=idx_min, description=f'{sequence}_test', time=blender_time)
    
    # run solution
    #!tracking/libct/setup-dev-env
    res = run_procedure(f'ct {local_input}')
    
    # copy to results
    shutil.copyfile(local_output, source_solution)
    
    assert os.path.isfile(source_solution)
    
    print('source file', source_file)
    print('source solution', source_solution)
    
    os.remove(local_input)
    os.remove(local_output)
    
    
    
def parse_vertex_id(vertex_id):
    
    frame_idx, label = vertex_id[-10:-5], vertex_id[-5:]
    return int(frame_idx), int(label)
    
    
def libct2ctc(data_path, res_path):
    '''
    Save labeled images in a format 'maskXXX.tif to the <res_path>
    
    Parameters:
    -----------
    data_path : str
        path where raw segmentations are stored
    res_path : str
        directory containing libct.sol and tracking.txt
        also output directory
        
    
    Returns:
    --------
    
    None
    
    '''
    
    # remove all the files from the res_path directory
    for file in os.listdir(res_path):
        if 'tracking' not in file:
            os.remove(os.path.join(res_path, file))
    
    # test if libct.sol exists
    sol_path = os.path.join(res_path, 'tracking.sol')
    assert os.path.isfile(sol_path), sol_path
    
    # read sol file
    edges_from, start_vertices = parse_sol_file(sol_path)
    
    tracker = DFSTracker(edges_from, start_vertices)
    label_map = tracker.dfs()
        
    # save ctc res_track.txt file
    tracker.save_ctc_tracking(res_path)
    
    
    translate_results(data_path, res_path, label_map)
    
    return label_map
    
    
def get_frame_idx(img_name):
    '''
    reads the first continuous sequence of digits in an image name
    '''
    res = ''
    for d in img_name:
        if d.isdigit():
            res = res + d
        elif res != '':
            break
    return int(res)
    
    
def translate_results(data_path, res_path, label_map):
    
    #print('storing res images')
    # iterate over images in res_path
    
    img_names = [n for n in os.listdir(data_path) if '.tif' in n]
    for img_name in tqdm(img_names):
        
        f_idx = get_frame_idx(img_name)
        
        # read original labeled image
        image_path = os.path.join(data_path, img_name)
        assert os.path.isfile(image_path)
        
        img = io.imread(image_path)
        
        #print(f'reading {image_path}, unique_values: {np.unique(img)}')
        
        res = np.zeros_like(img, dtype=np.uint16)
        
        mapping = label_map.get(f_idx, {})
        
        # TODO:
        # apply mapping as a lookup table
        #np.vectorize(mapping.get)(img)
        
        
        for old_label in mapping.keys():
            new_label = mapping[old_label]   
            res[img == old_label] = new_label
        
        # save result
        store_path = os.path.join(res_path, img_name)
        
        #print(f'saving {store_path}, unique_values: {np.unique(res)}')
        io.imsave(store_path,
                  res,
                  check_contrast=False,
                  plugin='tifffile',
                  compression ='zlib')
        
        

    
class DFSTracker():
    
    def __init__(self,
                edges_from,
                start_vertices):
        
        self.edges_from = edges_from
        self.start_vertices = start_vertices
    
    
        # DFS
        self.label_map = {} 
        # label_map = { f_idx : { old_label : new_label } }
        self.start_frames = {}
        # start_frames = { label : ( start_frame_idx, mother ) }
        self.ctc_tracking = []  # label start_frame, end_frame, mother_idx

    def dfs(self):
        new_label = 1
                
        # start all tracks from start vertices
        for v_id in self.start_vertices:

            f_idx, old_label = parse_vertex_id(v_id)
            mother_idx = 0
            
            # start new track
            self.start_frames[new_label] = (f_idx, mother_idx)
            self.update_label_map(f_idx, old_label, new_label)

            # recursive call
            new_label = self.dfs_rec(v_id, new_label, f_idx)
            
        return self.label_map
            
    def dfs_rec(self, v_id, label, f_idx):
        
        # move to the next frames
        edges = self.edges_from[v_id]
        
        # stop condition
        if (len(edges) == 1) and (edges[0] == 0):
            
            # add info to the tracking file
            self.close_track(label, f_idx)
            
            # increase the label value
            return label + 1
        
        # next iteration
        if len(edges) == 1:
            
            v_id_next = edges[0]
            frame_next, old_label = parse_vertex_id(v_id_next)
            assert frame_next == (f_idx + 1)
            
            # continue the track
            self.update_label_map(frame_next, old_label, label)
            return self.dfs_rec(v_id_next, label, frame_next)
            
        elif len(edges) == 2:
            
            # close current track
            self.close_track(label, f_idx)
            
            # new label index
            new_label = label + 1
            mother_idx = label
            
            for v_id_next in edges:
                
                frame_next, old_label = parse_vertex_id(v_id_next)
                assert frame_next == (f_idx + 1)
                
                # start new track
                self.start_frames[new_label] = (frame_next, mother_idx)
                
                # continue new track
                self.update_label_map(frame_next, old_label, new_label)
                new_label = self.dfs_rec(v_id_next, new_label, frame_next)
                
            return new_label
        else:
            assert False, f'improper successors {edges}'
        
        
        
    def update_label_map(self, f_idx, old_label, new_label):
        
        self.label_map[f_idx] = self.label_map.get(f_idx, {})
        self.label_map[f_idx][old_label] = new_label   
        
        
        
    def close_track(self, label, f_idx):
        
            start_frame, mother_idx = self.start_frames[label]
            tracklet = (str(label), str(start_frame), str(f_idx), str(mother_idx))
            self.ctc_tracking.append(tracklet)
            
            
    def save_ctc_tracking(self, res_path):
        
        path = os.path.join(res_path, 'res_track.txt')
        
        lines = [' '.join(list(tr)) + '\n' for tr in self.ctc_tracking]
        
        
        print(f'saving {path}')
        with open(path, 'w') as f:
            f.writelines(lines)
            
            
                
                
        


def parse_sol_file(sol_path):
    
    assert os.path.isfile(sol_path), sol_path
    
    # process tracking result
    # create dictionary path_to
    edge_from = {}
    start_vertices = []
    
    # every vertex is indexed as libct ID
    # f'{frame_idx:05d}{label:05d}'
    
    
    # iterate over 
    
    with open(sol_path, 'r') as f:
        
        for line in f.readlines():
            
            tokens = line.rstrip().split(' ')          
            
            if tokens[0] == 'H':
                v_idx = tokens[1][-10:]
                edge_from[v_idx] = []
            
            if tokens[0] == 'DISAPP':
                v_idx = tokens[1][-10:]
                
                # diappearing track is marked by 0
                edge_from[v_idx].append(0)
                
            if tokens[0] == 'APP':
                v_idx = tokens[1][-10:]
                
                # diappearing track is marked by 0
                start_vertices.append(v_idx)
                
            if tokens[0] == 'MOVE':
                v_idx_src = tokens[1][-21:-11]
                v_idx_dst = tokens[1][-10:]
                
                assert len(v_idx_src) == len(v_idx_dst) == 10
                
                edge_from[v_idx_src].append(v_idx_dst)
                
                
            if tokens[0] == 'DIV':
                v_idx_src = tokens[1][-32:-22]
                v_idx_ds1 = tokens[1][-21:-11]
                v_idx_ds2 = tokens[1][-10:]
                
                assert len(v_idx_src) == len(v_idx_ds1) == len(v_idx_ds2) == 10
                
                edge_from[v_idx_src].append(v_idx_ds1)
                edge_from[v_idx_src].append(v_idx_ds2)
                
                
    return edge_from, start_vertices
    
