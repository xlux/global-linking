"""
Author: Filip Lux (2023), Masaryk University
Licensed under MIT License
"""

from skimage import measure
import numpy as np
import pandas as pd

from .my_utils.image import Dataset

class FlowGraph():
    
    def __init__(self,
                 res_path):
        

        # unique indexes of source and termination vertices
        self.source_idx = 0  # source
        self.sink_idx = 1    # target

        # first cell index
        self.vertex_index = 2
        self.edge_index = 0
        
        self.vertices = {}   # vertices[f_idx][label] = v_idx
        
        
        # define res_path
        #res_path = Path(path, f'{sequence}_DATA')
        assert res_path.exists(), res_path
        
        # get datset of frames 
        self.dataset = Dataset(res_path, name_pattern='mask')


        '''
        #########
        OUTPUTS
        #########

        each vertex and edge has its index
        v_idx, e_idx
        using this index you can get vertices that it represents
        '''
        self.edges = {} # edges[e_idx] = (v_idx, v_idx)

        'vertex_map maps vetex id to location on a particular frame'
        self.vertex_map = {}  # maps v_idx to tuple (frame, x, y)

        'veiws to sets of edges'
        self.edge_from_me = {self.source_idx: [], self.sink_idx: []}     # edge_from[v_idx] = [e_idx, e_idx, ...]
        self.edge_to_me =   {self.source_idx: [], self.sink_idx: []}     # edge_to[v_idx] = [e_idx, e_idx, ...]

        '''
        'pd DataFrames to store events'
        EDGE_COLUMNS = ['edge_index', 'f0', 'f1', 'x0', 'y0', 'x1', 'y1']
        df_edges = pd.DataFrame(columns=MOVE_COLUMNS)
        df_vertices = pd.DataFrame(columns=MOVE_COLUMNS)
        '''

        # edge_df_rows = []

        self.vertex_prob = {0: 1., 1: 1.}
        self.edge_prob = {}
        
        'list of coos from the frames'
        self.coos = self.read_coos_file(res_path / 'vertex_prob.csv')
                    
            
    def read_coos_file(self, vertex_prob_path):
        
        if vertex_prob_path is None:
            return {}

        res = {}
        vertex_prob = pd.read_csv(vertex_prob_path)
        #vertex_prob = vertex_prob.drop(columns=['scores'])
        
        for _, row in vertex_prob.iterrows():
            
            if len(row) == 6:
                time, label, x, y, z, prob = row
                coo = (x, y, z)
            elif len(row) == 5:
                time, label, x, y, prob = row
                coo = (x, y)
            else:
                #print(f'UNPROPPER FORMAT OF CSV FILE {vertex_prob_path}')
                return {}
            
            if time not in res.keys():
                res[time] = {}
            res[time][label] = coo
            
        return res
            
            
                
        
    def _read_frame(self, frame_idx):
        
        frame = self.dataset.get_frame(frame_idx)
        
        res = {}
        for reg in measure.regionprops(frame):
            res[reg.label] = reg.centroid
            
        return res

    def read_coos(self, frame_idx, label):
        
        if frame_idx not in self.coos.keys():
            #print('ERROR: frame index not in coos.keys(): ', frame_idx, label, self.coos.keys())
            self.coos[frame_idx] = self._read_frame(frame_idx)
            
        coos = self.coos[frame_idx]
        
        assert label in coos.keys()
        
        return coos[label]

    def get_dictionary_from_masks(self):
        dataset = Dataset(self.res_path, name_pattern='mask')

    def add_vertex(self, frame_idx, label, prob):
        frame_idx, label = int(frame_idx), int(label)

        # get new index
        v_idx = self.vertex_index
        self.vertex_index += 1
        
        # create dict, if necessary
        if frame_idx not in self.vertices.keys():
            self.vertices[frame_idx] = {}
            
        # check
        assert label not in self.vertices[frame_idx].keys()
        
        self.vertices[frame_idx][label] = v_idx
        
        #if prob < .
        
        self.vertex_prob[v_idx] = prob
        
        # frame, label, x, y
        # TODO: add coordinates
        #            - contain dataset
        #            - load each frame only ones
        
        coo = self.read_coos(frame_idx, label)       #TODO: read from dictonary
        
        if len(coo) == 2:
            x, y = coo
            self.vertex_map[v_idx] = (frame_idx, label, x, y)
        elif len(coo) == 3:
            x, y, z = coo
            self.vertex_map[v_idx] = (frame_idx, label, x, y, z)
        
        # prepare edge containers
        self.edge_from_me[v_idx] = []
        self.edge_to_me[v_idx] = []
        
        return v_idx
        
    def add_edge(self, frame1, label1, frame2, label2, prob):
        
        frame1, label1, frame2, label2 = int(frame1), int(label1), int(frame2), int(label2)
        
        v1_idx = self.get_vertex_index(frame1, label1)
        v2_idx = self.get_vertex_index(frame2, label2)
        
        e_idx = self.edge_index
        self.edge_index += 1
        
        self.__add_edge(v1_idx, v2_idx, e_idx, prob)
        
        return e_idx
    
    def __add_edge(self, v1_idx, v2_idx, e_idx, prob):
        
        self.edges[e_idx] = (v1_idx, v2_idx)
        self.edge_prob[e_idx] = prob
        
        self.edge_from_me[v1_idx].append(e_idx)
        self.edge_to_me[v2_idx].append(e_idx)
    
    def add_source_edge(self, frame2, label2):
        
        frame2, label2 = int(frame2), int(label2)
        
        v2_idx = self.get_vertex_index(frame2, label2)
        
        e_idx = self.edge_index
        self.edge_index += 1
        
        self.__add_edge(self.source_idx, v2_idx, e_idx, 1.)
        
        return e_idx
    
    def add_sink_edge(self, frame1, label1):
        frame1, label1 = int(frame1), int(label1)
        
        v1_idx = self.get_vertex_index(frame1, label1)
        
        e_idx = self.edge_index
        self.edge_index += 1
        
        self.__add_edge(v1_idx, self.sink_idx, e_idx, 1.)
        
        return e_idx
    
    def get_vertex_index(self, frame, label):
        
        # appearance
        if label == 0:
            return self.source_idx 
        
        assert frame in self.vertices.keys(), f'{frame} {label}'
        assert label in self.vertices[frame].keys(), f'{frame} {label}'
        
        return self.vertices[frame][label]

    def get_distance(self, frame1, label1, frame2, label2):
        f_idx1 = self.get_vertex_index(frame1, label1)
        f_idx2 = self.get_vertex_index(frame2, label2)

        _, _, x1, y1 = self.vertex_map[f_idx1]
        _, _, x2, y2 = self.vertex_map[f_idx2]

        return np.linalg.norm(np.array([x2 - x1, y2 - y1]))

    def knn(self, frame_idx, x1, y1, n=3, limit_dist=100):
        
        # get v_idxs in the frame
        # for each compute distance (limit by x, y square
        # sort by distance
        # take first n
        
        all_dets = self.vertices[frame_idx].values()
        
        cands = {}
        for v_idx in all_dets:
            _, _, x2, y2 = self.vertex_map[v_idx]

            # speed optimization
            if (abs(x1 - x2) >= limit_dist) or (abs(y1 - y2) >= limit_dist):
                continue

            dist = np.linalg.norm(np.array([x2 - x1, y2 - y1]))
            if dist < limit_dist:
                cands[v_idx] = dist
                
        res = sorted(cands.items(), key=lambda x : x[1])[:n]
        return [key for key, _ in res]
