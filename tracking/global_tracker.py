"""
Author: Filip Lux (2023), Masaryk University
Licensed under MIT License
"""

import os
import math

import pandas as pd
import numpy as np
from tqdm import tqdm
from skimage import io
import math


from pathlib import Path
from .sys_tools import run_procedure
from .libct import run_libct, libct2ctc
from .graph import FlowGraph
from .divergence_tools import compute_divergence, compute_distance, compute_distance2, compute_distance3


from itertools import repeat, chain
from matplotlib import pyplot as plt




class GlobalTracker():
    
    '''
    one instance per a sequence
    operates in a one directory XX_DATA, where outputs of embedtrack are stored
    '''
    
    
    def __init__(self,
                 data_path,
                 res_path,
                 mean_dist=10,
                 max_dist=20,        # lambda_2
                 vertex_thr=0.95,    # lambda_T
                 normalize=False,
                 n_edges=3,
                 min_frame=0,
                 max_frame=2000,
                 distance_strategy='markers',
                 ):
        '''
        Parameters:
        -----------
        data_path : str
            path to the raw data
            schema of the directory:
            
            <data_path>/01
                       /01_GT
                       
                       # added directories
                       /01_RES                 # symlink to <res_path>/01_RES
                       /01_DATA/mask0000.tif   # output of the segmentation procedure
                               /...
                               /tra_offsets.csv
                               /...
                       ...
        res_path : str
            path to the stored results
            
            <res_path>/01_RES/tracking.txt      # self.run_tracking()
                             /libct.sol
                             /...
                             /mask0000.tif      # self.libctsol2ctc()          
                             /...
                             /res_track.txt     # self.evaluate_ctc()
                             /...
        '''
        
        sequence = os.path.basename(data_path)[:2]
        #assert sequence == os.path.basename(res_path)[:2]
            
        self.res_path = res_path
        self.data_path = data_path
        assert os.path.isdir(self.data_path), f'ERROR: directory {self.data_path} does not exist.'
        
        self.input_shape = self._get_input_shape()
        
        self.graph = None
        self.sequence = sequence
        self.mean_dist = mean_dist
        self.max_dist = max_dist
        self.vertex_thr = vertex_thr
        
        
        # refers to the edge_prob_distance.csv
        self.distance_strategy = distance_strategy
        assert self.distance_strategy in ['markers', 'displacement' ]

        
        self.n_edges = n_edges
        self.normalize = normalize
        
        
        self.min_frame = min_frame
        self.max_frame = max_frame
        
        self.graph = self.create_graph()
                
        
    def create_graph(self):
        # create graph
        # TODO: limit to min_frame/max_frame
        print('creating graph')
        return get_graph_bk(
                        self.data_path,
                        min_frame=self.min_frame,
                        max_frame=self.max_frame,
                        n_edges=self.n_edges,
                        distance_strategy=self.distance_strategy)
        

    def run_tracking(self,
                     idx_min=0,
                     idx_max=10000,
                     limit_dist=30,
                     zero_exp=300,
                     thetaS=1,
                     lambda_2=40,
                     lambda_1=40,
                     ):
        
        # compute distances
        div_path = os.path.join(self.data_path, 'edge_prob_distance2.csv')
            
        assert os.path.isfile(div_path)
        

        # create and save tracking file
        print('creating tracking file')
        tracking = graph2tracking_bk(self.graph,
                                     lbd1=lambda_1,
                                     lbd2=lambda_2,                     # max move distance 
                                     lbdT=self.vertex_thr,                   # vertex threshold
                                     thetaS=thetaS,               # not used when None
                                     limit_dist=limit_dist,
                                     zero_exp=zero_exp,
                                     input_shape=self.input_shape,
                                     )
        
        save_tracking(tracking, self.res_path)

        # compute solution
        print('computing solution')
        run_libct(self.res_path) 
        
    
    def save_results_ctc(self):
        
        # translate libct.sol to ctc result format
        lm = libct2ctc(data_path=self.data_path,
                       res_path=self.res_path)
        return lm  
    
    def solution_stats(self, plot=False):
        '''
        outputs 'solution_stats.txt' file, describing
        a contribution of individual items on a solution
        '''
        
        tracking_file_path = Path(self.res_path, 'tracking.txt')
        sol_file_path = Path(self.res_path, 'tracking.sol')
        assert os.path.isfile(tracking_file_path), tracking_file_path
        assert os.path.isfile(sol_file_path), sol_file_path
        
        sol_stats(sol_file_path, tracking_file_path, plot=plot)
        
        
    def evaluate_ctc(self,
                     measures=('TRAMeasure', 'DETMeasure', 'SEGMeasure')):
        '''
        computes CTC metrics to evaluate the results
        '''
        
        # TODO: test if the data_path containt GT directory
        
        dirname = os.path.dirname(self.data_path)
        directory = os.path.basename(self.res_path)
        
        gt_path = os.path.join(dirname, directory.replace('RES', 'GT'))
        if not os.path.isdir(gt_path):
            print(f'ground truth masks are not available {gt_path}')
            return
        
        temp_res_path = os.path.join(os.getcwd(), dirname, directory)
        src_path = os.path.join(os.getcwd(), self.res_path)
        
        print(src_path)
        
        # try to remove symlink
        run_procedure(f'rm {temp_res_path}')
        
        assert not Path(temp_res_path).exists(), f'the target folder already exists {temp_res_path}'
        assert Path(src_path).exists()
        
        # create symlink from results to original data folder
        run_procedure(f'ln -s {src_path} {temp_res_path}')
        
        # run evaluation procedures
        #measures = ['TRAMeasure', 'DETMeasure', 'SEGMeasure']
        #measures = ['TRAMeasure', 'SEGMeasure']
        for m in measures:
            command = f'./ctc_metrics/{m} {dirname} {self.sequence} 4'
            print(f'running {command}')
            run_procedure(command)
            
        # remove symlink
        run_procedure(f'rm {temp_res_path}')
        
    def report_ctc_measures(self, parameters):
        """
        for a given parameters, finds a resuts of measures
        """
        
        print(f'parameters :{parameters}')
        for file in os.listdir(self.res_path):
            
            if file[-7:] == 'log.txt':
                print(f'{file}: {read_last_line(os.path.join(self.res_path, file))}')
                
                
    def _get_input_shape(self):
        assert os.path.isdir(self.data_path), f'ERROR: directory {self.data_path} does not exist.'    
        
        images = [name for name in os.listdir(self.data_path) if '.tif' == name[-4:]]
        
        assert len(images) > 0, 'There are no images in '
        img = io.imread(os.path.join(self.data_path, images[0]))
        
        return img.shape
                
def read_last_line(file_path):

    with open(file_path) as f:
        for line in f:
            pass
        return line
        
        
    

def compute_edge_cost(log_kl_divergence, distance, avg_dist=10):

    # A) cost depends only on a distance of objects
    cost = distance - avg_dist


    # B) combine time 5
    #cost = cost + np.log1p(np.maximum((dist - avg_dist)**3, 0))

    #cost = cost - (((dist - avg_dist) / 9) **3)

    return - cost


def get_graph_bk(
        data_path,
        n_edges=5,
        min_frame=0,
        max_frame=2000,
        distance_strategy='markers'
    ):
    '''
    creates instance of candidate graph
    
    Parameters
    ----------
    data_path: str
        path to the dataset
    n_edges: int
        graph connects only <n_edges> of the closest neighbour vertices 
    min_frame: int
        first frame index to analyze (included)
    max_frame: int 
        last frame index to analyze (included)
        
    Returns
    -------
    
    graph: tuple
        structured data about the graph
    
    '''
    
    # create an empty graph structure
    graph = FlowGraph(data_path)
    
    # decide based on the available files, what version to use
    # 1) use only position of detections
    #          - compute_distance3
    # 2) compare center of marker and predicted position
    #          - compute_distance2
    # 3) compare predicted positions and actual positions
    #          - compute_distance
    
    # compute distances
    if distance_strategy == 'markers':
        
        pd_edge_path = os.path.join(data_path, 'edge_prob_distance3.csv')
        measure = 'distance_markers'
        
        if not os.path.isfile(pd_edge_path):
            print('computing distance\n using only positions of markers')
            compute_distance3(data_path)
            
    elif distance_strategy == 'displacement':
        seg_offsets_path = os.path.join(data_path, 'seg_offsts.csv')
        tra_offsets_path = os.path.join(data_path, 'tra_offsts.csv')
        
        assert os.path.isfile(seg_offsets_path), seg_offsets_path
        assert os.path.isfile(tra_offsets_path), tra_offsets_path
        
        
        pd_edge_path = os.path.join(data_path, 'edge_prob_distance2.csv')
        measure = 'distance'
        
        if not os.path.isfile(pd_edge_path):
            print('computing distance\n using only positions of markers and their displacement')
            compute_distance2(data_path)
    else:
        assert False, 'ERROR: invalid strategy to compute intervertex distance'

        

    # read info about the detections and high probability edges
    pd_vertex_path = os.path.join(data_path, 'vertex_prob.csv')
    assert os.path.isfile(pd_edge_path), pd_edge_path
    assert os.path.isfile(pd_vertex_path), pd_vertex_path

    df_edges = pd.read_csv(pd_edge_path, index_col=False)
    df_vertices = pd.read_csv(pd_vertex_path, index_col=False)
    
    # list of real frame indexes
    frame_indexes = df_vertices['time'].unique()
    
    # limit problem in temporal domain
    frame_indexes.sort()   # TODO: to remove
    min_frame = int(np.maximum(frame_indexes.min(), min_frame))
    max_frame = int(np.minimum(frame_indexes.max(), max_frame))
    
    ########## 
    # ADD ALL VERTICES
    print('GET GRAPH: adding vertices')
    for i, vertex in df_vertices.iterrows():

        prob = vertex['prob']
        label = vertex['label']
        frame_idx = vertex['time']
        
        # limit frames that are processed
        if not (min_frame <= frame_idx <= max_frame):
            continue
        
        assert label > 0
        
        graph.add_vertex(frame_idx, label, prob)
        
        ########## 
        # ADD SOURCE AND SINK EDGES
        if int(frame_idx) == min_frame:
            graph.add_source_edge(frame_idx, label)
            
        elif int(frame_idx) == max_frame:
            graph.add_sink_edge(frame_idx, label)
        
    ###########
    # ADD ALL MIDDLE EDGES
    print('GET GRAPH: adding edges')
        
    # iterate over every timeframe
    for time_curr in df_edges.time_curr.unique():
        
        # limit frames that are processed
        if (time_curr <= min_frame) or (time_curr > max_frame):
            continue
        
        # iterate over all edges
        time_curr_df_mask = df_edges.time_curr == time_curr
        for label_curr in df_edges[df_edges.time_curr == time_curr].label_curr.unique():
            
            view = df_edges[(time_curr_df_mask) & (df_edges.label_curr == label_curr)].nsmallest(n_edges, measure)

            for _, edge in view.iterrows():
                
                dist = edge[measure]
                label_curr = edge['label_curr']
                frame_curr = edge['time_curr']
                label_prev = edge['label_prev']
                frame_prev = edge['time_prev']               

                # no appearance in the sequence
                graph.add_edge(frame_curr, label_curr, frame_prev, label_prev, dist)
        

    ''' OUTPUT '''
    indexes = graph.vertex_index, graph.edge_index
    probs = graph.edge_prob, graph.vertex_prob
    move_edges = graph.edge_to_me, graph.edge_from_me

    return indexes, probs, graph.edges, move_edges, graph.vertex_map


def softmax(arr):
    return np.exp(arr) / np.sum(np.exp(arr))

def sigmoid(x):
    return 1 / (1 + math.exp(-x))


def get_distance_cost(dist,
                      mean_dist=10,
                      max_dist=20,
                      min_cost=-1):
    #dist = np.linalg.norm(np.array([x2 - x1, y2 - y1]))
    #return -max((dist - max_dist) / (max_dist - mean_dist), min_cost)
    # TODO: include mean_dist
    return - (dist * min_cost / max_dist - min_cost)
    #return ((dist/max_dist)**2 - 1) * (-min_cost)


# GRAPH TO TRACKING
# KL
def vertex_cost(prob,
                vertex_thr=0.95,
                min_cost=5):
    '''
    probs of real vertices is higher than .95
    '''
    # TODO: extravagant hand-made metrics
    #       replace 
    
    return - min_cost / (1 - vertex_thr) * ( prob - vertex_thr)


# DEPRECIATED
def edge_cost(prob, x1, y1, x2, y2, lbd=.5, mean_dist=10, max_dist=20, min_cost=-1):
    '''
    edge cost is log(divergence)
    '''

    dist = np.linalg.norm(np.array([x2 - x1, y2 - y1]))
    dst_cost = - get_distance_cost(dist, mean_dist, max_dist, min_cost)
    dvg_cost = - prob
        
    return lbd * dst_cost + (1 - lbd) * dvg_cost


def graph2tracking_bk(graph,
                      lbd1=40,
                      lbd2=15,               # max move distance
                      lbdT=0.95,             # appearance / disappearance in the first / last thT frames
                      thetaS=1, 
                      limit_dist=50,
                      zero_exp=300,
                      input_shape=None,
                      z_anisotropy=5,         # suppose (1, 1, 5)
                      
):
    """
    translates structure of graph to libct file tracking.txt
    translates prob and dist to costs
    """
    
    CLOSE_TO_ZERO = 10**-zero_exp
        
    # decompose graph
    indexes, probs, edges, move_edges, vertex_map = graph

    vertex_index, edge_index = indexes
    edge_prob, vertex_prob = probs
    edge_to, edge_from = move_edges
        
    # variables
    tracking = [] 

    last_frame_index = np.max([val[0] for val in vertex_map.values()])
    first_frame_index = np.min([val[0] for val in vertex_map.values()])
    
    frame_shift = first_frame_index
    
    # vertex hypothesis
    # iterate over vertex_map
    
    for v_idx in tqdm(vertex_map.keys()):
        
        val = vertex_map[v_idx]
        frame_idx, label = val[0], val[1]
        coos = val[2:]
        
        # TODO: use a parameter 0.95 to classify better vertices
        
        prob0 = vertex_prob[v_idx]
        
        # map vertex prob
        prob = sigmoid((prob0 - lbdT) * lbd1)
        
        # compute cost
        cost = 10 * (-np.log(2*prob))
        
        unique_id = f'1{frame_idx:05d}{label:05d}' 
        assert len(unique_id) == 11, unique_id
        
        '''
        # FIXED VERTEX PROB experiment
        if prob0 > THR_VERTEX_PROB:
            cost = -35
        else:
            cost = 350000
        '''
        #cost = 0
        
        if len(coos) == 2:
            x, y = coos
            tracking.append(f'H {frame_idx-frame_shift} {unique_id} {cost} {x} {y}')
        elif len(coos) == 3:
            x, y, z = coos
            tracking.append(f'H {frame_idx-frame_shift} {unique_id} {cost} {x} {y} {z}')
        else:
            tracking.append(f'H {frame_idx-frame_shift} {unique_id} {cost}')
        
        app_prob = CLOSE_TO_ZERO
        disapp_prob = CLOSE_TO_ZERO
        
        # TODO:
        # include position, lambda_2
        
        # app/disapp on the first/last frame is for free
        
        min_dist_to_domain_edge = get_min_dist(input_shape, coos, z_anisotropy)
        
        # spatial appearance and disappearance
        if thetaS > min_dist_to_domain_edge:
            app_prob = np.exp(- min_dist_to_domain_edge / lbd2) 
            disapp_prob = np.exp(- min_dist_to_domain_edge / lbd2) 
            
        # temporal appearance and disappearance
        if ( frame_idx == first_frame_index):
            app_prob = 1
        if ( frame_idx == last_frame_index):
            disapp_prob = 1
            
        # APP/DISAPP costs
        app_cost = - np.log(app_prob)
        disapp_cost = - np.log(disapp_prob)

        # add appearance and disappearance
        unique_id_app = f'4{frame_idx:05d}{label:05d}' 
        unique_id_dis = f'5{frame_idx:05d}{label:05d}' 
        tracking.append(f'APP {unique_id_app} {unique_id} {app_cost}')
        tracking.append(f'DISAPP {unique_id_dis} {unique_id} {disapp_cost}')
        
        # additional conflict set
        #if prob0 >= THR_VERTEX_PROB:
        #    tracking.append(f'CONFSET {unique_id} <= 1')

    edges_ = {}
    
    for e_idx in edges.keys():
        
        v_idx_curr, v_idx_prev  = edges[e_idx]
        
        # sink and source vertices
        if v_idx_prev in [0, 1]:
            continue
            
        if v_idx_curr == 0:
            continue
    
        
        val_prev = vertex_map[v_idx_prev]
        val_curr = vertex_map[v_idx_curr]
        frame_idx_prev, label_prev = val_prev[0], val_prev[1]
        frame_idx_curr, label_curr = val_curr[0], val_curr[1]
        
        dist = edge_prob[e_idx]
        
        prob = np.exp(-dist / lbd2)
        edge_cost = -np.log(2*prob) 
        
        # limit long edges
        if edge_cost > limit_dist :
            continue

        # create the edge descriptor
        seg_id_right = f'1{frame_idx_curr:05d}{label_curr:05d}' 
        seg_id_left = f'1{frame_idx_prev:05d}{label_prev:05d}' 
        unique_id = f'2{seg_id_left}{seg_id_right}'
        assert len(unique_id) == 23, unique_id
        
        # update tracking
        tracking.append(f'MOVE {unique_id} {seg_id_left} {seg_id_right} {edge_cost}')

        # propagate info to compute division events later on
        edges_[seg_id_left] = edges_.get(seg_id_left, [])
        edges_[seg_id_left].append((seg_id_right, edge_cost))

    # division
    div_idx = 0
    for id_left in tqdm(edges_.keys(), 'divisions'):
        
        right_ids = edges_[id_left]
        
        # TODO: find three cheapest edges
        right_ids.sort(key=lambda a: a[1])
        right_ids = right_ids[:3]
                
        for id_right1, cost1 in right_ids:
            for id_right2, cost2 in right_ids:
    
                if id_right1 >= id_right2:
                    continue
                    
                # define unique id
                unique_id = f'3{id_left}{id_right1}{id_right2}'
                assert len(unique_id) == 34, unique_id
                
                cost_div = (cost1 + cost2) / 2
                
                # to reduce a candidate tree size
                if cost_div  > 1:
                    continue
                
                tracking.append(f'DIV {unique_id} {id_left} {id_right1} {id_right2} {cost_div}')
                                
                div_idx += 1
                
    print(f'Added {div_idx} division candidates.')
    return tracking


def get_min_dist(img_shape,
                 coos,
                 z_anisotropy):
    
    if len(coos) < 2:
        return 10000         # a constant high enough to disallow appearance or disappearance
    
    assert len(img_shape) == len(coos)
    
    anisotropy_scale = np.ones_like(coos)
    if (z_anisotropy is not None) and (len(coos) > 2):
        anisotropy_scale[2] = z_anisotropy
    
    
    min_dist = max(img_shape)
    for lim_val, val, sc in zip(img_shape, coos, anisotropy_scale):
        min_dist = min(val*sc, (lim_val-val)*sc, min_dist)
        
    return min_dist
        


def save_tracking(tracking, data_path):
    
    tracking_path = os.path.join(data_path, 'tracking.txt')
    with open(tracking_path, 'w', encoding='utf8') as f:
        
        line_end = repeat("\n")
        lines = chain.from_iterable(zip(tracking, line_end))
        f.writelines(lines)
        
        lines = chain.from_iterable(zip(tracking, line_end))
        print(lines)
        
        div, move = 0, 0
        hyp = set()
        
        for line in lines:
            tokens = line.split(' ')
            
            if tokens[0] == 'H':
                hyp.add(tokens[2])
                
            
            if tokens[0] == 'MOVE':
                time1 = tokens[2][1:6]
                time2 = tokens[3][1:6]
                
                assert tokens[2] in hyp
                assert tokens[3] in hyp

                assert int(time1) + 1 == int(time2), f'not correct {line}'
                
                move += 1
                
            if tokens[0] == 'DIV':
                time1 = tokens[2][1:6]
                time2 = tokens[3][1:6]
                time3 = tokens[4][1:6]
                
                assert tokens[2] in hyp
                assert tokens[3] in hyp
                assert tokens[4] in hyp

                assert int(time1) + 1 == int(time2), f'not correct {line}'
                assert int(time1) + 1 == int(time3), f'not correct {line}'
                
                div += 1
                
        print(f'CORRECT, {move} {div}')
        
        
def sol_stats(sol_path,
              tra_path,
              plot=False):
    '''
    Displays candidate graph 

    Parameters
    ----------

    sol_path : str
        path to 'tracking.sol' file, in libct format
    tra_path : str
        path to 'tracking.txt' file, in libct format
    plot : bool
        plot the histograms

    '''
    

    assert os.path.isfile(tra_path), tra_path
    assert os.path.isfile(sol_path), sol_path
    dirname = os.path.dirname(tra_path)
    
    # COLORS 
    
    # get vertex map from 'tracking.txt'
    vertex_map = {} # vertex_id : (x, y, frame_idx)
    
    tra = {'H': {},
           'APP': {},
           'DISAPP': {},
           'MOVE': {},
           'DIV': {}}
    
    all_tra = {'H': [],
               'APP': [],
               'DISAPP': [],
               'MOVE': [],
               'DIV': []}
        
    with open(tra_path, 'r') as f:
        

        
        # show vertices
        for line in tqdm(f.readlines()):
            tokens = line.split(' ')
            key = tokens[0]
            if key == 'H':
                unique_id, cost = tokens[2], tokens[3]
            elif key == 'APP':
                unique_id, cost = tokens[1], tokens[3]
            elif key == 'DISAPP':
                unique_id, cost = tokens[1], tokens[3]
            elif key == 'MOVE':
                unique_id, cost = tokens[1], tokens[4]
            elif key == 'DIV':
                unique_id, cost = tokens[1], tokens[5]
            else:
                continue
                
            tra[key][unique_id] = cost
            all_tra[key].append(cost)
            
            
    sol = {'H': [],
           'APP': [],
           'DISAPP': [],
           'MOVE': [],
           'DIV': []}
                
    with open(sol_path, 'r') as f:
        
        # show vertices
        for line in tqdm(f.readlines()):
            
            key, unique_id = line.rstrip().split(' ')
            cost = tra[key][unique_id]
            
            sol[key].append(cost)
            
    dir_name = os.path.dirname(sol_path)
    res_path = os.path.join(dir_name, 'sol_stats.txt')
    
    
    
    
    with open(res_path, 'w') as f:
        
        lines = []
        
        # show vertices
        for key in sol.keys():
            
            # tra
            vals = np.array(all_tra[key], dtype=float)
            line = f'TRA {str(key)} {vals.sum()} {len(vals)} {vals.min()} {vals.mean()} {vals.max()}\n'
            
            lines.append(line)
            
            # sol
            vals = np.array(sol[key], dtype=float)
            if len(vals) > 0:
                line = f'SOL {str(key)} {vals.sum()} {len(vals)} {vals.min()} {vals.mean()} {vals.max()}\n'

                lines.append(line)
        f.writelines(lines)
        
    if plot:
        plt.figure()
        
        for key in sol.keys():
            
            vals = np.array(sol[key], dtype=float)
            plt.hist(vals,
                     histtype='step',
                     density=True,
                     bins=100)
        #plt.legend(sol.keys)
        plt.title('tracking.sol')
        plt.xlim((-5, 5))
        plt.ylim((0, 5))
        plt.savefig(os.path.join(dirname, 'sol.png'))
        plt.show()
        
        
        plt.figure()
        
        for key in sol.keys():
            
            vals = np.array(all_tra[key], dtype=float)
            plt.hist(vals,
                     histtype='step',
                     density=True,
                     bins=100)
        #plt.legend(sol.keys())
        plt.title('tracking.txt')
        plt.xlim((-5, 5))
        plt.ylim((0, 5))
        plt.savefig(os.path.join(dirname, 'tra.png'))
        plt.show()

        

        
            
        
