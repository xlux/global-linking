from .sys_tools import run_procedure


# TODO: communicate with an embedtrack  a config file
def run_embedtrack(data_path, seq, model_path, batch_size=1):

    prompt = f'python3 EmbedTrack/embedtrack.py --sequence {seq} --data_path {data_path} --model_path {model_path} --batch_size {batch_size}'
    run_procedure(prompt)

