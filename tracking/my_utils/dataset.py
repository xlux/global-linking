"""
Author: Filip Lux (2023), Masaryk University
Licensed under MIT License
"""

import numpy as np
import tensorflow as tf
import pandas as pd
import os
import re

from my_utils.image import get_split_sample, get_move_sample, get_cell_sizes, Dataset
from skimage.exposure import equalize_hist

import time


DF_COLUMNS = ['dataset', 'sequence', 'frame1', 'frame2', 'mx', 'my', 's1x', 's1y', 's2x', 's2y', 'true']
OFFSET = 30

class SplitDataGenerator(tf.keras.utils.Sequence):
    'Generates data for Keras'
    def __init__(self,
                 split_path,
                 nonsplit_path,
                 batch_size=32,
                 dim=(160,160),
                 n_channels=2,
                 offset=30,
                 shuffle=True):
        'Initialization'
        self.dim = dim
        # assert batch_size % 2 == 0
        self.batch_size = batch_size
        # self.class_batch_size = batch_size // 2
        self.split_pd = pd.read_csv(split_path, index_col = False, dtype={'sequence':str})
        self.nonsplit_pd = pd.read_csv(nonsplit_path, index_col = False, dtype={'sequence':str})
        self.n_channels = n_channels
        self.offset = offset
        self.shuffle = shuffle
        self.class_names = ['nonsplit', 'split']
        self.cell_sizes = self._get_cell_sizes()
        self.samples = []
        self.on_epoch_end()

    def __len__(self):
        'Denotes number of batches per epoch'
        return int(np.floor(len(self.samples)/self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data, clases are balanced'

        # Find list of IDs
        list_IDs = self.samples[index*self.batch_size:(index+1)*self.batch_size]
        
        assert len(list_IDs) == self.batch_size

        # Generate data
        X, y = self.__data_generation(list_IDs)

        return X, y
    
    def take(self, index):
        return [self[index]]
    
    def _get_cell_sizes(self):
        cell_sizes = {}
        datasets = np.unique(list(self.split_pd['dataset'].unique()) + list(self.nonsplit_pd['dataset'].unique()))
                
        for dataset in datasets:
            cell_sizes[dataset] = int(np.mean(get_cell_sizes(dataset)) + self.offset)
        return cell_sizes

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.i1 = np.arange(len(self.split_pd))
        self.i0 = np.arange(len(self.nonsplit_pd))
        if self.shuffle == True:
            np.random.shuffle(self.i1)
            np.random.shuffle(self.i0)
        half_lenght = min(len(self.split_pd), len(self.nonsplit_pd))
        self.samples = [(1, self.i1[k]) for k in range(half_lenght)] + [(0, self.i0[k]) for k in range(half_lenght)]
        np.random.shuffle(self.samples)

    def __data_generation(self, list_IDs):
        'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
        # Initialization
        X = np.ones((self.batch_size, *self.dim, self.n_channels), dtype=float) * 0.5
        y = np.empty((self.batch_size), dtype=int)

        # Generate data
        for i, (gt_class, row_index) in enumerate(list_IDs):
            
            # DF_COLUMNS = ['dataset', 'sequence', 'frame1', 'frame2', 'mx', 'my', 's1x', 's1y', 's2x', 's2y', 'true']
            if gt_class == 0:
                row = self.nonsplit_pd.iloc[row_index]
            else:
                row = self.split_pd.iloc[row_index]
            assert row.true == gt_class
            event = (row.mx, row.my, row.s1x, row.s1y, row.s2x, row.s2y)
            img_dataset = Dataset(os.path.join(row.dataset, row.sequence))
            sample = get_split_sample(img_dataset,
                                            row.frame1,
                                            row.frame2,
                                            event,
                                            self.cell_sizes[row.dataset],
                                            shape=self.dim,
                                            viz=False)
            

            # Store sample
            X[i] = sample
            y[i] = row.true

        return X, tf.keras.utils.to_categorical(y, num_classes=2)
    
    
class MoveDataGenerator(tf.keras.utils.Sequence):
    'Generates data for Keras'
    def __init__(self,
                 move_path,
                 nonmove_path,
                 batch_size=32,
                 dim=(160,160),
                 n_channels=2,
                 offset=30,
                 shuffle=True):
        'Initialization'
        self.dim = dim
        # assert batch_size % 2 == 0
        self.batch_size = batch_size
        # self.class_batch_size = batch_size // 2
        self.split_pd = pd.read_csv(move_path, index_col = False, dtype={'sequence':str})
        self.nonsplit_pd = pd.read_csv(nonmove_path, index_col = False, dtype={'sequence':str})
        self.n_channels = n_channels
        self.offset = offset
        self.shuffle = shuffle
        self.class_names = ['nonsplit', 'split']
        self.cell_sizes = self._get_cell_sizes()
        self.samples = []
        self.on_epoch_end()

    def __len__(self):
        'Denotes number of batches per epoch'
        return int(np.floor(len(self.samples)/self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data, clases are balanced'

        # Find list of IDs
        list_IDs = self.samples[index*self.batch_size:(index+1)*self.batch_size]
        
        assert len(list_IDs) == self.batch_size

        # Generate data
        X, y = self.__data_generation(list_IDs)

        return X, y
    
    def take(self, index):
        return [self[index]]
    
    def _get_cell_sizes(self):
        cell_sizes = {}
        datasets = np.unique(list(self.split_pd['dataset'].unique()) + list(self.nonsplit_pd['dataset'].unique()))
                
        for dataset in datasets:
            cell_sizes[dataset] = int(np.mean(get_cell_sizes(dataset)) + self.offset)
        return cell_sizes

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.i1 = np.arange(len(self.split_pd))
        self.i0 = np.arange(len(self.nonsplit_pd))
        if self.shuffle == True:
            np.random.shuffle(self.i1)
            np.random.shuffle(self.i0)
        half_lenght = min(len(self.split_pd), len(self.nonsplit_pd))
        self.samples = [(1, self.i1[k]) for k in range(half_lenght)] + [(0, self.i0[k]) for k in range(half_lenght)]
        np.random.shuffle(self.samples)

    def __data_generation(self, list_IDs):
        'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
        # Initialization
        X = np.ones((self.batch_size, *self.dim, self.n_channels), dtype=float) * 0.5
        y = np.empty((self.batch_size), dtype=int)

        # Generate data
        for i, (gt_class, row_index) in enumerate(list_IDs):
            
            # COLUMNS = ['dataset', 'sequence', 'frame1', 'frame2', 'mx', 'my', 'sx', 'sy', 'true']
            if gt_class == 0:
                row = self.nonsplit_pd.iloc[row_index]
            else:
                row = self.split_pd.iloc[row_index]
            assert row.true == gt_class
            event = (row.mx, row.my, row.sx, row.sy)
            img_dataset = Dataset(os.path.join(row.dataset, row.sequence))
            sample = get_move_sample(img_dataset,
                                            row.frame1,
                                            row.frame2,
                                            event,
                                            self.cell_sizes[row.dataset],
                                            shape=self.dim,
                                            viz=False)
            

            # Store sample
            X[i] = sample
            y[i] = row.true

        return X, tf.keras.utils.to_categorical(y, num_classes=2)
    
    
    
class PredictDataGenerator(tf.keras.utils.Sequence):
    'Generates data for Keras'
    def __init__(self,
                 dataframe,
                 dataset,
                 sequence,
                 mode,
                 batch_size=32,
                 dim=(160,160),
                 n_channels=3,
                 offset=30,
                 shuffle=False):
        'Initialization'
        self.dim = dim
        self.dataset = Dataset(os.path.join(dataset, sequence))
        assert mode in ['move', 'split'], f'Invalid mode: {mode}'
        self.mode = mode
        self.batch_size = batch_size
        self.predict_pd = dataframe ##pd.read_csv(pd_path, index_col = False)
        self.n_channels = n_channels
        self.offset = offset
        self.shuffle = shuffle
        self.cell_size = int(np.mean(get_cell_sizes(dataset)) + self.offset)
        self.samples = []
        self.on_epoch_end()

    def __len__(self):
        'Denotes number of batches per epoch'
        assert len(self.samples) > 0
        return int(np.floor((len(self.samples) - 1)/self.batch_size)) + 1

    def __getitem__(self, index):
        'Generate one batch of data, clases are balanced'

        # Find list of IDs
        list_IDs = self.samples[index*self.batch_size:(index+1)*self.batch_size]
        
        # Generate data
        X = self.__data_generation(list_IDs)

        return X

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.samples = np.arange(len(self.predict_pd))

    def __data_generation(self, list_IDs):
        'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
        # Initialization
        X = np.ones((len(list_IDs), *self.dim, self.n_channels), dtype=float) * 0.5        
        
        ## t0= time.time()

        # Generate split data
        for i, row_index in enumerate(list_IDs):

            # COLUMNS = ['dataset', 'sequence', 'frame1', 'frame2', 'mx', 'my', 'sx', 'sy', 'true']
            row = self.predict_pd.iloc[row_index]
            if self.mode == 'split':
                event = (row.mx, row.my, row.xd1, row.yd1, row.xd2, row.yd2)
                sample = get_split_sample(self.dataset,
                                          row.f0,
                                          row.f1,
                                          event,
                                          self.cell_size,
                                          shape=self.dim,
                                          viz=False)
            elif self.mode == 'move':
                event = (row.x0, row.y0, row.x1, row.y1)
                sample = get_move_sample(self.dataset,
                                         row.f0,
                                         row.f1,
                                         event,
                                         self.cell_size,
                                         shape=self.dim,
                                         viz=False)

            # Store sample
            X[i] = sample
            
        return X