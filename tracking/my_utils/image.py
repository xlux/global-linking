"""
Author: Filip Lux (2023), Masaryk University
Licensed under MIT License
"""

import math
import os
import time

import re


from matplotlib import pyplot as plt
import numpy as np
from skimage import io
from skimage import morphology
from skimage.measure import label, regionprops
from skimage.morphology import reconstruction, opening
from skimage.transform import rotate, rescale
from scipy.stats import norm, gaussian_kde, gamma
from skimage.exposure import equalize_hist
from skimage.draw import disk
import tifffile as ff


from tqdm import tqdm

class Dataset3d:
    def __init__(self,
                 data_path,
                 name_pattern='',
                 max_size=None):
        
        assert os.path.isdir(data_path), data_path
        self._path = data_path
        self._files = sorted([os.path.join(self._path, name) for name in os.listdir(self._path) \
                              if '.tif' in name and name_pattern in name])
        self._max_size = max_size
        self._len = len(self._files)
    
    def get(self, index):
        if index >= self._len:
            return None

        return ff.imread(self._files[index])
    
    def get_name(self, index):
        return self._files[index]
    
    def shape(self):
        return self.get(0).shape
    
    def __len__(self):
        if self._max_size:
            return min(self._max_size, self._len)
        return self._len
    
    
    def get_conflict_limit(self):
        
        m, n, o = self.shape()
        minimal_dist = m*n*o
        
        print('>> get conflict limit <<')
        for i in tqdm(range(self._len)):
            
            coos = {}
            frame = self.get(i)
            regions = regionprops(frame)
            for r in regions:
                coos[r.label] = r.centroid
                
            for label_i in coos:
                for label_j in coos:
                    if label_i < label_j:
                        dist = distance(coos[label_i], coos[label_j])
                        if dist < minimal_dist:
                            minimal_dist = dist
        
        return minimal_dist


class Dataset:
    def __init__(self,
                 data_path,
                 name_pattern='',
                 max_size=None):
        
        assert os.path.isdir(data_path), data_path
        self._path = data_path
        self._files = sorted([os.path.join(self._path, name) for name in os.listdir(self._path) \
                              if '.tif' in name and name_pattern in name])
        self._max_size = max_size
        self._len = len(self._files)
        self._frame_indicies = sorted([int(re.sub("\D", "", os.path.basename(path))) for path in self._files])
    
    def get(self, index):
        if index >= self._len:
            return None
        # print(self._files[index])
        return io.imread(self._files[index])
    
    def get_frame(self, frame_idx):
        if frame_idx not in self._frame_indicies:
            return None

        file_name = self._files[self._frame_indicies.index(int(frame_idx))]        
        assert str(frame_idx) in file_name, f'{frame_idx} not in {file_name}'
        return io.imread(file_name)
    
    def get_name(self, index):
        return self._files[index]
    
    def shape(self):
        return self.get(0).shape
    
    def __len__(self):
        if self._max_size:
            return min(self._max_size, self._len)
        return self._len
    
    
    def get_conflict_limit(self):
        
        m, n = self.shape()
        minimal_dist = m*n
        
        print('>> get conflict limit <<')
        for i in tqdm(range(self._len)):
            
            coos = {}
            frame = self.get(i)
            regions = regionprops(frame)
            for r in regions:
                coos[r.label] = r.centroid
                
            for label_i in coos:
                for label_j in coos:
                    if label_i != label_j:
                        dist = distance(coos[label_i], coos[label_j])
                        if dist < minimal_dist:
                            minimal_dist = dist
        
        return minimal_dist
                
        
    

def get_coos(mask):
    ''' returns coordinates of all connected components in mask'''
        
    coos = {}
    
    measures = regionprops(label(mask))
    for m in measures:
        coos[m['label']] = m['centroid']
        
    return coos
    
    
def get_init_markers(img, threshold=224):
    ''' gets markersd as connected components, higher as threshold'''
    mask = img > threshold
    
    coos = get_coos(mask)
    return coos


def get_init_markers_rec(img, threshold=128):
    ''' Gets markers as local maximas, higher as threshold'''
    img = np.maximum(img.astype(int) - threshold, 0)
    seed = np.maximum(img.astype(int) - 1, 0)
    
    mask = img - reconstruction(seed, img) 
    
    coos = get_coos(mask)
    return coos



def get_matrix(coos1, coos2, d_max=50):
    
    keys1 = sorted(coos1.keys())
    keys2 = sorted(coos2.keys())
    
    matrix = np.ones((len(keys1) * 2, len(keys2) * 2 ))
    
    # set dummy values
    matrix *= d_max
    
    for i, key1 in enumerate(keys1):
        for j, key2 in enumerate(keys2):
            x1, y1 = coos1[key1]
            x2, y2 = coos2[key2]
            vector = (x1 - x2, y1 - y2)
            dist = vector_len(vector)            
            matrix[i, j] = dist
            
    return matrix
        


class Split:
    def __init__(self, m_coo, s1_coo, s2_coo):
        self.m_x, self.m_y = m_coo
        self.s1_x, self.s1_y = s1_coo
        self.s2_x, self.s2_y = s2_coo
        
        self.s0_x = (self.s1_x + self.s2_x) / 2
        self.s0_y = (self.s1_y + self.s2_y) / 2
        
    def get_shift_vector(self):
        sx = self.s0_x - self.m_x
        sy = self.s0_y - self.m_y
        return (sx, sy)
    
    
    def get_s1_vector(self):
        sx = self.s1_x - self.s0_x
        sy = self.s1_y - self.s0_y
        return (sx, sy)
    
    def get_s2_vector(self):
        sx = self.s2_x - self.s0_x
        sy = self.s2_y - self.s0_y
        return (sx, sy)
        
        

def vector_len(vector):
    """ Returns lenght of the vector.  """
    return np.linalg.norm(vector)


def vector_distances(vectors, point, max_dist=100):
    """ Returns dist of point to list of vectors """
    result = []
    x, y = point
    for v in vectors:
        m, n = v
        v1, v2 = x-m, y-n
        if abs(v1) + abs(v2) > max_dist:
            continue
        vector = (v1, v2)
        result.append(np.linalg.norm(vector))
        
    return result
    
    
def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)


def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))


def get_pdf(data):

    mean,std=norm.fit(data)

    plt.hist(data, bins=50, density=True)
    xmin, xmax = plt.xlim()
    x = np.linspace(xmin, xmax, 100)
    y = norm.pdf(x, mean, std)
    plt.plot(x, y)
    plt.show()
    return mean, std


def get_regions(frame,
                gt=False,
                threshold=128):
    if not gt: 
        return get_regions_local_maxima(frame,
                                       threshold=threshold)
    else:
        return regionprops(frame)
    
    
def get_regions_local_maxima(img,
                             threshold=128):
    ''' Gets markers as local maximas, higher as threshold'''
    img = np.maximum(img.astype(int) - threshold, 0)
    seed = np.maximum(img.astype(int) - 1, 0)
    
    mask = img - reconstruction(seed, img) 
    
    coos = get_coos(mask)
    regions = [{'centroid': coos[key]} for key in coos.keys()]
    
    return regions


def get_regions_h_max(img,
                      h=128):
    ''' Gets markers as local maximas, higher as threshold'''
    seed = np.maximum(img.astype(int) - h, 0)
    
    mask = (img - reconstruction(seed, img) == h) * 1
    
    coos = get_coos(mask)
    regions = [{'centroid': coos[key]} for key in coos.keys()]
    
    return regions
    
    
# 3. get samples stats

DF_COLUMNS = ['dataset', 'sequence', 'frame1', 'frame2', 'mx', 'my', 's1x', 's1y', 's2x', 's2y', 'true']


def get_rotation_center(event):
    if len(event) == 6:
        mx, my, s1x, s1y, s2x, s2y = event
        cx, cy = (s1x + s2x + mx) // 3, (s1y + s2y + my) // 3
    elif len(event) == 4:
        mx, my, sx, sy = event
        cx, cy = (sx + mx) // 2, (sy + my) // 2
    return int(cx), int(cy)

def get_rotation_angle(event):
    if len(event) == 6:
        _, _, s1x, s1y, s2x, s2y = event
        return np.degrees(np.arctan2(s1x - s2x, s1y - s2y))
    if len(event) == 4:
        mx, my, sx, sy = event
        return np.degrees(np.arctan2(mx - sx, my - sy))

def rotate_coo(coo, center, angle):
    x, y = coo
    cx, cy = center
    
    vx, vy = x - cx, y - cy
    
    lenght = np.sqrt(vx ** 2 + vy ** 2)
    beta = np.arctan2(vy, vx) + np.deg2rad(angle)
    
    dx, dy = math.cos(beta) * lenght, math.sin(beta) * lenght
    return dx + cx, dy + cy


def get_safe_range(array,
              boundary_mult=1.2):
    
    'cut out outliers'
    array = np.array(array)
    array = array[array < (np.mean(array) + 10*np.std(array))]
    
    a_max, a_min = np.max(array), np.min(array)
    
    assert a_min >= 0
    assert boundary_mult >= 1
    
    if a_max == a_min:
        return max(a_min - 1, 0), a_max + 1
    
    midd = (a_max + a_min) / 2
    new_half_range = (midd - a_min) * boundary_mult
    
    new_min = max(midd - new_half_range,  0)
    new_max = midd + new_half_range
    
    assert new_min < new_max
    
    return new_min, new_max


def get_normal_distribution(array):
    
    'cut out outliers'
    array = np.array(array)
    array = array[array < (np.mean(array) + 10*np.std(array))]
    
    
    mean = np.mean(array)
    std = np.std(array)
    
    return mean, std


def get_gamma_distribution(array):
    
    'cut out outliers'
    array = np.array(array)
    array = array[array < (np.mean(array) + 10*np.std(array))]
    
    alpha, loc, beta = gamma.fit(array)
    
    return alpha, loc, beta
    

def get_split_events(datasets,
               sequences,
               viz=False,
               expand=1.2):
    '''
    returns DataFrame of locations to read samples for cell split training
    '''
    df = pd.DataFrame(columns=DF_COLUMNS)
    
    for name in datasets:

        cell_size = int(np.mean(get_cell_sizes(name)) * expand)
        print(f'Dataset {name} has mean cell radius {cell_size}.')
        dcs = cell_size // 2

        for seq in sequences:
            img_path = os.path.join(name, seq)
            img_d = Dataset(img_path)

            m, n = img_d.shape()

            split = split_coordinates[name][seq]
            print(name, seq, len(split))

            for frame_id in split.keys():
                split_events = split[frame_id]
                for event in split_events:
                    mx, my, s1x, s1y, s2x, s2y = event

                    if viz:
                        img_m, img_s = get_split_sample(name,
                             seq,
                             frame_id-1,
                             frame_id,
                             event,
                             cell_size)

                        fig, (ax1, ax2) = plt.subplots(1, 2)
                        ax1.imshow(img_m)
                        ax2.imshow(img_s)
                        plt.show()
                   ## ['dataset', 'sequence', 'frame1', 'frame2', 'mx', 'my', 's1x', 's1y', 's2x', 's2y', 'true']
                    row = pd.DataFrame([[name, seq, frame_id-1, frame_id, mx, my, s1x, s1y, s2x, s2y, 1]], columns=DF_COLUMNS)
                    df = df.append(row, ignore_index=True)

    return df

def crop(img, bbx_coo):
    'crop image with mirroring of pixels outside the domain'
    
    assert len(bbx_coo) == 4
    x0, y0, x1, y1 = bbx_coo
    
    assert x0 < x1 and y0 < y1, bbx_coo
    
    out_mx, out_my = x1 - x0, y1 - y0
    out = np.zeros((out_mx, out_my, 3))
    
    assert len(out.shape) == 3, out.shape
    assert len(img.shape) == 3, img.shape
    
    mx, my, _ = img.shape
    # 1. valid
    # 'X_low' is a number of non-valid pixels in indexes lower than zero
    # 'X_high' is a number of non-valid pixels in indexes higher than image size
    x_low, y_low = max(-x0, 0), max(-y0, 0)
    x_high, y_high = max(x1 - mx, 0), max(y1 - my, 0)
    
    x0, y0 = max(x0, 0), max(y0, 0)
    x1, y1 = min(x1, mx), min(y1, my)
    
    out[x_low:out_mx-x_high, y_low:out_my-y_high, :] =  img[x0:x1, y0:y1, :]
    
    # mirroring edges
    # 2. x mirror
    '''
    out[:x_low, :] = out[2*x_low:x_low:-1, :]
    if x_high > 0:
        out[-x_high:, :] = out[-x_high-1:out_mx-2*x_high-1:-1, :]
    
    # 3. y mirror
    out[:, :y_low] = out[:, 2*y_low:y_low:-1]
    if y_high > 0:
        out[:, -y_high:] = out[:, -y_high-1:out_my-2*y_high-1:-1]
    '''
    assert len(out.shape) == 3, out.shape
    return out


def draw_event(img_mother, img_sons, event):
    out_m = img_mother.copy()
    out_s = img_sons.copy()
    
    assert len(event) == 6
    mx, my, s1x, s1y, s2x, s2y = event

    out_m[disk((mx, my), 10)] = 255
    out_s[disk((s1x, s1y), 10)] = 255
    out_s[disk((s2x, s2y), 10)] = 255

    return out_m, out_s


def rotate_event(event):
    angle = get_rotation_angle(event)
    cx, cy = get_rotation_center(event)
    
    if len(event) == 6:
        mx, my, s1x, s1y, s2x, s2y = event
        mx_r, my_r = rotate_coo((mx, my), (cx, cy), angle)
        s1x_r, s1y_r = rotate_coo((s1x, s1y), (cx, cy), angle)
        s2x_r, s2y_r = rotate_coo((s2x, s2y), (cx, cy), angle)
        return mx_r, my_r, s1x_r, s1y_r, s2x_r, s2y_r
    if len(event) == 4:
        mx, my, sx, sy = event
        mx_r, my_r = rotate_coo((mx, my), (cx, cy), angle)
        sx_r, sy_r = rotate_coo((sx, sy), (cx, cy), angle)
        return mx_r, my_r, sx_r, sy_r
    
def crop_coordinates(cell_size, coo):
    
    assert len(coo) == 2
    cx, cy = coo
    
    x0, y0 = int(cx - cell_size // 2), int(cy - cell_size // 2)
    x1, y1 = int(x0 + cell_size), int(y0 + cell_size)
    
    assert x0 < x1 and y0 < y1
    
    return x0, y0, x1, y1

def get_sample_old(dataset,
               sequence,
               id1,
               id2,
               event,
               cell_size,
               shape=(224, 224),
               viz=False):
    
    
    x, y = shape
    scale = (x / cell_size, y / cell_size)
    
    gt_path = os.path.join(dataset, f'{sequence}')

    cx, cy = get_rotation_center(event)
    angle = get_rotation_angle(event)
    
    # get Dataset and analyze
    img_d = Dataset(gt_path)
    img_mother = img_d.get(id1)
    img_sons = img_d.get(id2)
    
    if viz:
        img_mother, img_sons = draw_event(img_mother, img_sons, event)
        
    img_merge = np.zeros((*img_mother.shape, 3))
    img_merge[:, :, 0] = img_mother
    img_merge[:, :, 1] = img_sons
    img_merge_r = rotate(img_merge, angle,
                      center=(cy, cx),
                      mode='reflect')

    event_r = rotate_event(event)
    
    # crop
    assert len(img_merge_r.shape) == 3
    img_crop = crop(img_merge_r, crop_coordinates(cell_size, (cx, cy)))
    
    # rescale
    sx, sy = x / img_crop.shape[0], y / img_crop.shape[1]
    patch = rescale(img_crop, scale=(sx, sy, 1))
    
    return patch[:, :, 0], patch[:, :, 1]


def distance(a, b):
    'distance of vectors a and b'
    vector = tuple(map(lambda i, j: i - j, a, b))
    lenght = vector_len(vector)

    return lenght


def get_distance(event):
    if len(event) == 4:
        ax, ay, bx, by = event
        return distance((ax, ay), (bx, by))
    elif len(event) == 6:
        mx, my, ax, ay, bx, by = event
        cx, cy = (ax + bx) / 2, (ay + by) / 2
        return distance((ax, ay), (bx, by)) + distance((mx, my), (cx, cy)) 
    
    
def get_move_dist(dataset_path,
                 sequences=['01', '02']):
    
    assert os.path.isdir(dataset_path)
    
    move_dist = []
    
    for seq in sequences:
        gt_path = os.path.join(dataset_path, f'{seq}_GT', 'TRA')
        assert os.path.isdir(gt_path)
       
        gt_d = Dataset(gt_path)

        coo = {}
        
        print('>> get move dist <<')

        for i in tqdm(range(len(gt_d))):
            
            coo, prev_coo = {}, coo
            img = gt_d.get(i)
            assert img is not None, i

            reg = regionprops(img)
            for r in reg:
                key = r['label']
                x1, y1 = r['centroid']
                if key in prev_coo:
                    x0, y0 = prev_coo[key]
                    move_dist.append(vector_len((x1 - x0, y1 - y0)))
                coo[key] = x1, y1

    return move_dist





def get_split_sample(img_dataset,
               id1,
               id2,
               event,
               cell_size,
               shape=(224, 224),
               viz=False):

    x, y = shape
    scale = (x / cell_size, y / cell_size)

    cx, cy = get_rotation_center(event)
    angle = get_rotation_angle(event)
    dist = get_distance(event)
    
    # get images
    img_mother = img_dataset.get(id1)
    img_sons = img_dataset.get(id2)
    
    if viz:
        img_mother, img_sons = draw_event(img_mother, img_sons, event)
        

    img_merge = np.ones((*img_mother.shape, 3)) * dist
    img_merge[:, :, 0] = equalize_hist(img_mother)
    img_merge[:, :, 1] = equalize_hist(img_sons)
    img_merge_r = rotate(img_merge, angle,
                      center=(cy, cx),
                      mode='reflect')

    event_r = rotate_event(event)

    # crop
    assert len(img_merge_r.shape) == 3
    cx0, cy0, cx1, cy1 = crop_coordinates(cell_size, (cx, cy))

    img_crop = crop(img_merge_r, (cx0, cy0, cx1, cy1 ))
    
    # rescale
    sx, sy = x / img_crop.shape[0], y / img_crop.shape[1]
    patch = rescale(img_crop, scale=(sx, sy, 1))
    
    return patch


def get_move_sample(img_dataset,
               id1,
               id2,
               event,
               cell_size,
               shape=(224, 224),
               viz=False):
    
    
    x, y = shape
    scale = (x / cell_size, y / cell_size)
    

    cx, cy = get_rotation_center(event)
    angle = get_rotation_angle(event)
    
    # get samples
    img_mother = img_dataset.get(id1)
    img_sons = img_dataset.get(id2)
        
    mx, my, sx, sy = event
    dist = get_distance(event)
        
    
    mother_crop = crop(np.expand_dims(img_mother, axis=2), crop_coordinates(cell_size, (mx, my)))
    sons_crop = crop(np.expand_dims(img_sons, axis=2), crop_coordinates(cell_size, (sx, sy)))
    
    img_merge = np.ones((cell_size, cell_size, 3)) * dist

    # rescale
    scale_m_x, scale_m_y = x / mother_crop.shape[0], y / mother_crop.shape[1]
    scale_s_x, scale_s_y = x / sons_crop.shape[0], y / sons_crop.shape[1]
    
    img_merge = np.ones((shape[0], shape[1], 3)) * dist
    m_rescale = rescale(mother_crop, scale=(scale_m_x, scale_m_y, 1))
    s_rescale = rescale(sons_crop, scale=(scale_s_x, scale_s_y, 1))
    

    img_merge[:, :, 0] = equalize_hist(m_rescale[:, :, 0])
    img_merge[:, :, 1] = equalize_hist(s_rescale[:, :, 0])
    
    return img_merge


def get_move_dst(img_dataset,
               id1,
               id2,
               event,
               cell_size,
               shape=(224, 224),
               viz=False):

    return get_distance(event)


def get_split_dst(img_dataset,
               id1,
               id2,
               event,
               cell_size,
               shape=(224, 224),
               viz=False):

    
    return get_distance(event)


# analyze cell size
def get_cell_sizes_frame(frame):
    
    result = []
    measures = regionprops(frame)
    for m in measures:
        x0, y0, x1, y1 = m['bbox']
        result.append(np.max([x1 - x0, y1 - y0]))

    return result

def get_cell_sizes(name):
    print(name)
    seqs = ['01', '02']
    sizes = []
    for seq in seqs:
            
        gt_path = os.path.join(name, f'{seq}_GT', 'SEG')
        assert os.path.isdir(gt_path)
        
        d_gt = Dataset(gt_path, name_pattern='seg')
        
        for i in range(len(d_gt)):
            frame = d_gt.get(i)
            sizes += get_cell_sizes_frame(frame)
        
    return sizes  


def get_cell_size(name, offset):
    return int(np.mean(get_cell_sizes(name)) + offset)


def get_cell_move_limit(name, offset):


        cell_size = int(np.mean(get_cell_sizes(name)) + offset)
        print(f'Dataset {name} has mean cell radius {cell_size}.')
        
        
        return int(np.sqrt(2) * cell_size) / 2
    
def get_split_limit(name, offset):
        cell_size = int(np.mean(get_cell_sizes(name)) + offset)
        print(f'Dataset {name} has mean cell radius {cell_size}.')
        
        return int(np.sqrt(2) * cell_size) / 2
        
        

def main():
    pass

if __name__ == "__main__":
    main()


   
    
    
