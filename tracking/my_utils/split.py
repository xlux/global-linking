"""
Author: Filip Lux (2023), Masaryk University
Licensed under MIT License
"""

## read and parse tracking file
import math
import os
import numpy as np
from tqdm.notebook import tqdm
from my_utils.image import Dataset, vector_len
from skimage.measure import regionprops

from scipy.stats import norm


class Split:
    def __init__(self, m_coo, s1_coo, s2_coo):
        self.m_x, self.m_y = m_coo
        self.s1_x, self.s1_y = s1_coo
        self.s2_x, self.s2_y = s2_coo
        
        self.s0_x = (self.s1_x + self.s2_x) / 2
        self.s0_y = (self.s1_y + self.s2_y) / 2
        
    def get_shift_vector(self):
        sx = self.s0_x - self.m_x
        sy = self.s0_y - self.m_y
        return (sx, sy)
    
    
    def get_s1_vector(self):
        sx = self.s1_x - self.s0_x
        sy = self.s1_y - self.s0_y
        return (sx, sy)
    
    def get_s2_vector(self):
        sx = self.s2_x - self.s0_x
        sy = self.s2_y - self.s0_y
        return (sx, sy)


def get_cc(img, index):
    # returns centroid coordinates
    mother_mask = (img == index)

    cells = regionprops((mother_mask)*1)
    # assert len(cells) >= int(mother_index), f'{np.unique(img_t0)} {mother_index}'
    assert len(cells) == 1, len(cells)
    return cells[0]

def get_splits(gt_path):
    '''
    return format: 
    splits = {frame_id: [(mx, my, s1x, s1y, s2x, s2y), ...]}
    '''

    ## analyze current tracking
    d = Dataset(gt_path)
    restrack_path = os.path.join(gt_path, 'man_track.txt')

    assert os.path.isfile(restrack_path)

    xym = []
    xm = []
    ym = []
    splits = {}
    
    tmp_splits = {}

    neightbour_dist = []


    with open(restrack_path, 'r') as f:
        for line in tqdm(f):
            index, time, _, mother_index = line.strip().split(' ')
            index, time, mother_index = int(index), int(time), int(mother_index)
            if mother_index != 0:
                img_t1 = d.get(time)
                img_t0 = d.get(time-1)

                if not mother_index in np.unique(img_t0):
                    print('WARNING: unconsistent track')
                    print(f'time: {time-1}-{time}\n mother_index: {mother_index}')
                    break

                mother = get_cc(img_t0, mother_index)
                son = get_cc(img_t1, index)
                mx, my = mother['centroid']
                sx, sy = son['centroid']

                dx = sx - mx
                dy = sy - my
                
                if mother_index not in tmp_splits.keys():
                    tmp_splits[mother_index] = (mx, my, sx, sy)

                else:
                    mx_, my_, sx1, sy1 = tmp_splits[mother_index]
                    assert mx == mx_
                    assert my == my_
                    splits[time] = splits.get(time, []) + [(mx, my, sx1, sy1, sx, sy)]
    return splits


def get_split_dist_path(gt_path):
    
    splits = get_splits(gt_path)
    split_dist = []
    
    
    for key in splits.keys():
        for event in splits[key]:
            mx, my, dx0, dy0, dx1, dy1 = event

            split_dist.append(vector_len((mx - dx0, my - dy0)))
            split_dist.append(vector_len((mx - dx1, my - dy1)))
        
    return split_dist


def get_split_dist(dataset_path,
                   sequences=['01', '02']):
    
    assert os.path.isdir(dataset_path)
    
    split_dist = []
    
    for seq in sequences:
        gt_path = os.path.join(dataset_path, f'{seq}_GT', 'TRA')
        assert os.path.isdir(gt_path)
        
        split_dist += get_split_dist_path(gt_path)

    return split_dist
        

                