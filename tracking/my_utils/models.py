"""
Author: Filip Lux (2023), Masaryk University
Licensed under MIT License
"""

from image import Dataset, vector_len, get_pdf, get_regions, get_coos
from scipy.stats import norm, gaussian_kde
from skimage.measure import regionprops

from tqdm import tqdm
import numpy as np
import os


def get_move_stat(gt_path):
    '''
    returns paramters <mi> <std>, which defines gaussian density function
    of the expected cell position after MOVE event
    '''

    gt_d = Dataset(gt_path)

    x = []
    y = []
    coo = {}

    for i in tqdm(range(len(gt_d))):
        img = gt_d.get(i)
        
        assert img is not None, i

        reg = regionprops(img)
        for r in reg:
            key = r['label']
            x1, y1 = r['centroid']
            if key in coo:
                x0, y0 = coo[key]
                x.append(x1 - x0)
                y.append(y1 - y0)

            coo[key] = x1, y1


    ## create dists variable
    move_dist = []
    for i, j in zip(x, y):
        dist = vector_len((i, j))
        move_dist.append(dist)
        move_dist.append(-dist)
        
    mi, std = get_pdf(move_dist)

    return mi, std


def add_edge(edges, edges_rev, i, j):
    edges[i] = edges.get(i, []) + [j]
    edges_rev[j] = edges_rev.get(j, []) + [i]
    
    assert j in edges[i]
    assert i in edges_rev[j]
    
    
def get_cell_dist(index, gt_img):
    m, n = gt_img.shape
    mask = np.array(gt_img) == int(index)

    assert np.sum(mask) > 0, f'{index}:\n{np.unique(gt_img)}\n{np.unique(mask)}'
    
    regions = regionprops((mask)*1)
    assert len(regions) == 1
    x, y = regions[0]['centroid']
    
    dist = np.min([x, y, m-x, n-y])
    return dist


def get_graph_Turetken_2017(path, gt=True, limit=50):
    '''
    function reads image data from the <path> and translate it to a graph described in Turetken (2017)
    Input:
        path - path to directory to image data
        gt - true if input images are stored as segmentation (16bit, objects labeled by integers)
    Output:
        index - number of vertices
        c - weights of edges
        edges - dictionary of successors
        edges_rev - dictionary of predecessors
        vertex_map - assigns to each vertex tuple (f, x, y), where <f> is frameID and <x>, <y> vertex coordinates in this frame
    '''

    gt_d = Dataset(path)
    m, n = gt_d.shape()
    
    # indexes of source, division and termination
    s_i = 0  # source
    d_i = 1  # division
    t_i = 2  # termination

    # first cell index
    index = 3
    
    c = {}
    edges = {}
    edges_rev = {}
    vertex_map = {} # maps vertex ID to tuple (frame, x, y)
    
    prev_vertices = {}
    
    # 
    if gt:
        m_mi, m_std = get_move_stat(path)
        pdf_max = norm.pdf(m_mi, m_mi, m_std)
        
    else:
        assert False, 'automatic stats not implemented'
        
    for j in tqdm(range(len(gt_d))):
        
        # load source image
        frame = gt_d.get(j)
        assert frame is not None, j
        vertices = {}
        
        # get and priocess new regions
        regions = get_regions(frame, gt)
        for r in regions:
            curr_index = index  # vertex index
            index += 1
            
            x1, y1 = r['centroid']
            vertices[curr_index] = (x1, y1) 
            vertex_map[curr_index] = (j, x1, y1)
            
            dist = np.min([x1, y1, m-x1, n-y1])
            
            # f_sj, f_it
            c[(curr_index, t_i)] = 0
            c[(s_i, curr_index)] = 0
            # f_dj
            c[(d_i, curr_index)] = 0
                    
            for v in prev_vertices.keys():
                x0, y0 = prev_vertices[v]
                
                dist = vector_len([x1-x0, y1-y0])
                if dist < limit:
                    # f_ij
                    c[(v, curr_index)] = norm.pdf(dist, m_mi, m_std) / pdf_max
                    add_edge(edges, edges_rev, v, curr_index)
                    
        prev_vertices = vertices
    
    # 3. last frame - contains only end vertices    
    ends = list(vertices.keys())


    return index, c, edges, edges_rev, vertex_map


def get_splits(restrack_path, dataset):
    '''
    returns dictionary for each frame with coos of splits
    Input:
        restrack_path - path to file
        dataset - instance of Dataset, gt
    Output:
        splits - dicts of coos indexed by frames in the following format: splits = {frame: [(x1, y1), (x2, y2), ... ]}
    '''
    
    ends = {}
    mothers = set()
    
    assert os.path.isfile(restrack_path), restrack_path
    
    with open(restrack_path, 'r') as f:
        for line in tqdm(f):
            index, time1, time2, mother_index = line.strip().split(' ')
            
            if int(mother_index) > 0:
                mothers.add(int(mother_index))
            ends[int(index)] = int(time2)
            
    splits = {}     
    for m_index in mothers:
        f_index = ends[m_index]
        frame = dataset.get(f_index)
        coo = get_coos(frame == m_index)
        splits[f_index] = splits.get(f_index, []) + [coo[1]]
        
    return splits

def get_moves(dataset):
    '''
    returns dictionary for a frame 'i' with coos of first position and following position in frame 'i+1' 
    Input:
        dataset - instance of Dataset, gt
    Output:
        moves - dicts of coos indexed by frames in the following format: splits = {frame: [(x1, y1, x2, y2), ... ]}
    '''
    
            
    moves = {}     
    for f_index in range(len(dataset) - 1):
        frame0 = dataset.get(f_index)
        frame1 = dataset.get(f_index + 1)
        
        markers = np.intersect1d(np.unique(frame0), np.unique(frame1))
        
        for m_index in markers:
            if m_index == 0:
                continue
            coo0 = get_coos(frame0 == m_index)
            coo1 = get_coos(frame1 == m_index)
            x0, y0 = coo0[1]
            x1, y1 = coo1[1]
            
            moves[f_index] = moves.get(f_index, []) + [(x0, y0, x1, y1)]
        
    return moves



def main():
    # test Turetken 2017
    gt_path = '../Fluo-N2DL-HeLa/01_GT/TRA'
    dataset = Dataset(gt_path)
    
    restrack_path = '../Fluo-N2DL-HeLa/01_GT/TRA/man_track.txt'
    moves = get_splits(restrack_path, dataset)
    for i in moves:
        print(i, ' : ', moves[i])
    
if __name__ == "__main__":
    main()

