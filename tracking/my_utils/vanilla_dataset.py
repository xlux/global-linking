"""
Author: Filip Lux (2023), Masaryk University
Licensed under MIT License
"""

import numpy as np
import pandas as pd
import os
from my_utils.image import get_split_sample, get_move_sample, get_cell_sizes, Dataset
from skimage.exposure import equalize_hist

import time


class DataGenerator():
    'Generates data for general pupose'
    def __init__(self,
                 dataframe,
                 dataset,
                 sequence,
                 mode,
                 batch_size=32,
                 dim=(160,160),
                 n_channels=3,
                 offset=30,
                 shuffle=False):
        'Initialization'
        self.dim = dim
        self.dataset = Dataset(os.path.join(dataset, sequence))
        assert mode in ['move', 'split'], f'Invalid mode: {mode}'
        self.mode = mode
        self.batch_size = batch_size
        self.predict_pd = dataframe ##pd.read_csv(pd_path, index_col = False)
        self.n_channels = n_channels
        self.offset = offset
        self.shuffle = shuffle
        self.cell_size = int(np.mean(get_cell_sizes(dataset)) + self.offset)
        self.samples = []
        self.on_epoch_end()

    def __len__(self):
        'Denotes number of batches per epoch'
        assert len(self.samples) > 0
        return int(np.floor((len(self.samples) - 1)/self.batch_size)) + 1

    def __getitem__(self, index):
        'Generate one batch of data, clases are balanced'

        # Find list of IDs
        list_IDs = self.samples[index*self.batch_size:(index+1)*self.batch_size]
        
        # Generate data
        X = self.__data_generation(list_IDs)

        return X

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.samples = np.arange(len(self.predict_pd))

    def __data_generation(self, list_IDs):
        'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
        # Initialization
        X = np.ones((len(list_IDs), *self.dim, self.n_channels), dtype=float) * 0.5        
        
        ## t0= time.time()

        # Generate split data
        for i, row_index in enumerate(list_IDs):

            # COLUMNS = ['dataset', 'sequence', 'frame1', 'frame2', 'mx', 'my', 'sx', 'sy', 'true']
            row = self.predict_pd.iloc[row_index]
            if self.mode == 'split':
                event = (row.mx, row.my, row.xd1, row.yd1, row.xd2, row.yd2)
                sample = get_split_sample(self.dataset,
                                          row.f0,
                                          row.f1,
                                          event,
                                          self.cell_size,
                                          shape=self.dim,
                                          viz=False)
            elif self.mode == 'move':
                event = (row.x0, row.y0, row.x1, row.y1)
                sample = get_move_sample(self.dataset,
                                         row.f0,
                                         row.f1,
                                         event,
                                         self.cell_size,
                                         shape=self.dim,
                                         viz=False)

            # Store sample
            X[i] = sample
            
        return X