# Global Cell Linking

Project to provide Global linking of cell detections.
It is currently connected with an EmbedTrack segmentation routine.


***

## Prerequisities

- conda ![link](https://conda.io/projects/conda/en/latest/user-guide/install/index.html)
- Gurobi Optimizer ![link](https://www.gurobi.com/downloads/gurobi-software/)

## Instalation


Clone the repository and update all its submodules. These are EmbedTrack an LibCT.


``` bash
git clone https://gitlab.fi.muni.cz/xlux/global-linking.git
cd global-linking
git submodule update --init --recursive

```

Then, create the conda evironment.

```bash
conda env create -f GlobalTracking_env.yml
conda activate GlobalTracking
```


Load dataset CTC `BF-C2DL-HSC` to directory `DATA`.  
The final structure should should be following:

```
# Structure of training data directory
--DATA
    -- 'SUBSET'                             # Subset is 'train' or 'challenge'
        -- 'DATASET_NAME'                   # Name of dataset e.g. 'BF-C2DL-HSC
            -- 01                           # YY - name of sequence, a two digit number, e.g. '01', '02'
                -- t0000.tif
                -- t0001.tif
                ...
            -- 01_GT                        # GT, 'train' subset only
                -- SEG                      # segmentation reference
                    -- man_seg0000.tif
                    -- man_seg0001.tif
                    ...
                -- TRA                      # tracking reference
                    -- man_track.txt
                    -- man_track0000.tif
                    -- man_track0001.tif
                      ...
```

Open the notebook

``` bash
./run_notebook.sh
```


## Description
The project adds global linking to the existing EmbedTrack method.
It is tested on a data from Cell Tracking Challenge (http://celltrackingchallenge.net/).

## Authors and Acknowledgment
Filip Lux

I hereby thank to Katharina Loffler for providing me a guidance through EmbedTrack method.

## License
MIT License

Copyright (c) 2023 Filip Lux

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

